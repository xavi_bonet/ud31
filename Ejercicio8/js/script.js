var x = ""; //número en pantalla
var y = ""; //guardar el numero que hay en la x al introducir un operador
var operacion = "no"; //operacion en curso, si 'no' significa que no hay operacion en curso.
var coma = false; //false = no hay coma, true si hay coma.

// Introducir un numero
function introducirNumero (numero) {
    if (x == "") {
        x = numero;
        $("#resultado").val(x);
    } else {
        x += numero;
        $("#resultado").val(x);
    }
}

// Introducir el operador
function operar(s) {
    igualar();
    y = x;
    $("#resultado").val("");
    x = "";
    operacion = s;
    coma = false;
}

// Realizar la operacion
function igualar() {
    if (operacion == "no") {
        //no ocurre nada.
    }else { 
        operacion = y+operacion+x;
        solucion = eval(operacion);
        $("#resultado").val(solucion);
        x = solucion;
        operacion = "no";
    }
}

$(document).ready(function(){
    // Borrar la ultima cifra o coma introducida
    $("#retr").click(function(){
        cifras = x.length;
        ultimaCifra = x.substr(cifras-1,cifras)
        x = x.substr(0,cifras-1)
        if (x == "") {
            x = "";
        }
        if (ultimaCifra == ".") {
            coma = false;
        }
        $("#resultado").val(x); 
    });

    // Borrar la ultima entrada
    $("#CE").click(function(){
        $("#resultado").val("");
        x = "";
        coma = false;
    });

    // Borrar todo
    $("#C").click(function(){
        $("#resultado").val("");
        x = "";
        y = "";
        coma = false;
        operacion="no" 
    });

    // Introducir una coma
    $("#coma").click(function(){
        if (x == "") {
            coma = true;
            x = "0.";
            $("#resultado").val(x);
        } else if (coma == false) {
            coma = true;
            x += ".";
            $("#resultado").val(x);
        }
    });

    // Calcular la raiz
    $("#raizc").click(function(){
        x = Math.sqrt(x);
        $("#resultado").val(x);
        operacion="no";
    });
    
    // Calcular porcentaje
    $("#porcent").click(function(){
        x = x/100;
        $("#resultado").val(x);
        igualar();
    });
    
    // Poner un numero en negativo o positivo
    $("#opuest").click(function(){
        var nx = Number(x);
        nx=-nx;
        x=String(nx);
        $("#resultado").val(x);
    });
    
    // Calcular 1/x
    $("#inve").click(function(){
        var nx=Number(x);
        nx = (1/nx);
        x = String(nx);		 
        $("#resultado").val(x);
    });
});